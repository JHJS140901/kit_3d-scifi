﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float rotSpeed;
    Rigidbody rb;
    bool canMove = true;
    bool flashLight = false;
    bool pickedUpItem = false;
    private string currentLocation;
    private string lastLocation;
    bool changeRoom = false;



    public Vector3 Bedroom = new Vector3(8,2,0);
    public Vector3 HallwayB = new Vector3(-9,2,-1);
    public Vector3 HallwayS = new Vector3(-2,2,4);
    public Vector3 StorageH = new Vector3(0,2,-9);
    public Vector3 StorageS = new Vector3(5,2,6);
    public Vector3 SecretRoom = new Vector3(-9,2,-6);

    private void Awake()
    {
        //DontDestroyOnLoad(this.gameObject);
        //Instantiate(this.gameObject);
    }

    // Start is called before the first frame update
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            Move();
        }
        ChangeRoom();
    }

    private void ChangeRoom()
    {
        if (currentLocation == "Bedroom")
        {
            transform.position = Bedroom;
            changeRoom = false;
        }
        if (currentLocation == "Hallway" && lastLocation == "Bedroom")
        {
            transform.position = HallwayB;
            changeRoom = false;
        }
        else if (currentLocation == "Hallway" && lastLocation == "Storage")
        {
            transform.position = HallwayS;
            changeRoom = false;
        }
        if (currentLocation == "Storage" && lastLocation == "Hallway")
        {
            transform.position = StorageH;
            changeRoom = false;
        }
        else if (currentLocation == "Storage" && lastLocation == "SecretRoom")
        {
            transform.position = StorageS;
            changeRoom = false;
        }
        if (currentLocation == "SecretRoom")
        {
            transform.position = SecretRoom;
            changeRoom = false;
        }
    }

    private void Move()
    {
        float x = Input.GetAxis("Horizontal") * Time.deltaTime * rotSpeed;
        float z = Input.GetAxis("Vertical") * Time.deltaTime * speed;
        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
    }

    public void CanMove(bool b)
    {
        canMove = b;
    }

    public bool FlashLight(bool b)
    {
        flashLight = b;
        return flashLight;
    }

    public bool PickedUpItem(bool b)
    {
        pickedUpItem = b;
        return pickedUpItem;
    }

    public bool ChangeRoom(bool b)
    {
        changeRoom = b;
        return changeRoom;
    }

    public string CurrentLocation(string s)
    {
        currentLocation = s;
        return currentLocation;
    }

    public string LastLocation(string s)
    {
        lastLocation = s;
        return lastLocation;
    }
}
