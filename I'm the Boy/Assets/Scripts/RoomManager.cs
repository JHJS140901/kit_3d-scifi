﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomManager : MonoBehaviour
{
    public string levelDoorConnector;
    public string currentRoom;
    public GameObject gameManager;

    PlayerManager getPlayer;
    GameObject player;
    PlayerController playerController;

    bool changedRoomOnce;
    string playerCurrentLocation = "Bedroom";
    string lastLocation = "none";

    private void Start()
    {
        getPlayer = gameManager.GetComponent<PlayerManager>();
        player = getPlayer.GivePlayer();
        playerController = player.GetComponent<PlayerController>();
        playerCurrentLocation = playerController.CurrentLocation(levelDoorConnector);
        lastLocation = playerController.LastLocation(currentRoom);
    }

    private void OnTriggerStay(Collider other)
    {
        if(Input.GetKeyDown(KeyCode.E) && other.tag == "Player")
        {
            changedRoomOnce = true;
            if(changedRoomOnce == true)
            {
                SceneManager.LoadScene(levelDoorConnector);
                playerController.CurrentLocation(levelDoorConnector);
                playerController.LastLocation(currentRoom);
                playerController.ChangeRoom(changedRoomOnce);
                changedRoomOnce = false;
            }
        }
    }

}
