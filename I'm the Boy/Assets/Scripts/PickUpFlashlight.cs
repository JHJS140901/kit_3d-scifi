﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpFlashlight : MonoBehaviour
{
    public GameObject gameManger;
    public GameObject flashlightBox;
    public GameObject flashlight;
    public GameObject flashlightCplayer;
    PlayerManager getPlayer;
    GameObject player;
    PlayerController playerController;

    // Start is called before the first frame update
    private void Start()
    {
        getPlayer = gameManger.GetComponent<PlayerManager>();
        player = getPlayer.GivePlayer();
        playerController = player.GetComponent<PlayerController>();
        flashlight = player.transform.GetChild(0).gameObject;
        flashlightCplayer = GameObject.FindGameObjectWithTag("Player").transform.GetChild(0).gameObject;
        flashlightCplayer.SetActive(false);
        flashlight.SetActive(false);
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E) && other.tag == "Player")
        {
            playerController.FlashLight(true);
            flashlightBox.SetActive(false);
            flashlight.SetActive(true);
            flashlightCplayer.SetActive(true);
            Debug.Log("true");
        }
    }
}
