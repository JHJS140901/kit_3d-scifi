﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenuGameManager : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene("Bedroom");
    }

    public void CloseGame()
    {
        Application.Quit();
    }
}
