﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UseItem : MonoBehaviour
{
    public GameObject gameManger;
    PlayerManager getPlayer;
    GameObject player;
    PlayerController playerController;
    bool pickedUp;
    public string winningScene;

    // Start is called before the first frame update
    private void Start()
    {
        getPlayer = gameManger.GetComponent<PlayerManager>();
        player = getPlayer.GivePlayer();
        playerController = player.GetComponent<PlayerController>();
        pickedUp = playerController.PickedUpItem(pickedUp);
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E) && other.tag == "Player")
        {
            playerController.PickedUpItem(false);
            Debug.Log("false");
            Debug.Log("used");
            SceneManager.LoadScene(winningScene);
        }
    }
}
