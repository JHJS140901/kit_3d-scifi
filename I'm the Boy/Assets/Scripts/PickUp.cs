﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public GameObject Object;
    public GameObject gameManager;
    PlayerManager getPlayer;
    GameObject player;
    PlayerController playerController;

    // Start is called before the first frame update
    private void Start()
    {
        getPlayer = gameManager.GetComponent<PlayerManager>();
        player = getPlayer.GivePlayer();
        playerController = player.GetComponent<PlayerController>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            playerController.PickedUpItem(true);
            Object.SetActive(false);
            Debug.Log("picked up final item");
        }
    }
}
