﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoad : MonoBehaviour
{
    PlayerManager playerManager;
    GameObject player;


    // Start is called before the first frame update
    void Start()
    {
        playerManager = gameObject.GetComponent<PlayerManager>();
        player = playerManager.GivePlayer();
        Instantiate(player, new Vector3(0, 1, 0), new Quaternion(0, 0, 0, 0));
        player.SetActive(true);
    }

}
