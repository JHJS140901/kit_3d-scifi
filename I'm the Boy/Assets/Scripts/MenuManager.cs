﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject button;

    public GameObject gameManger;

    PlayerManager getPlayer;
    PlayerController playerController;

    GameObject player;

    public Canvas canvas;

    private void Start()
    {
        getPlayer = gameManger.GetComponent<PlayerManager>();
        player = getPlayer.GivePlayer();
        playerController = player.GetComponent<PlayerController>();
    }

    public void OpenMenu()
    {
        canvas.gameObject.SetActive(true);
        button.SetActive(false);
        playerController.CanMove(false);
    }

    public void CloseMenu()
    {
        canvas.gameObject.SetActive(false);
        button.SetActive(true);
        playerController.CanMove(true);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitLevel()
    {
        SceneManager.LoadScene("StartScene");
    }
}
